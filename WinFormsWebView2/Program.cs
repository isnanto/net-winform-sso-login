﻿using IdentityModel.OidcClient;
using System;
using System.Text;
using System.Windows.Forms;

namespace WinFormsWebView2
{
    static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

         
    }
}
